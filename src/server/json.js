const csvToJson = require("convert-csv-to-json");

let fileInputNameMatches = "/home/venkat/javascript practise/nagalli_ipl_project/src/server/matches.csv"
let fileInputNameDeliveries = "/home/venkat/javascript practise/nagalli_ipl_project/src/server/deliveries.csv"

let matches = csvToJson.fieldDelimiter(",").getJsonFromCsv(fileInputNameMatches);
let deliveries = csvToJson.fieldDelimiter(",").getJsonFromCsv(fileInputNameDeliveries);

//console.log(matches)
//console.log(deliveries)

module.exports = {matches: matches, deliveries: deliveries};