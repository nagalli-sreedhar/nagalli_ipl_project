
 const iplDataSet = require("./json");
const fs = require("fs");
 const { PassThrough } = require("stream");
const path = require("path");
const { log } = require("console");
const { resourceLimits } = require("worker_threads");

let matches = iplDataSet.matches;
let deliveries = iplDataSet.deliveries;

// const match2015 = matches.filter((item) => {
//     if(item["season"] === "2015"){
//         return item;
//     }  
// })
// let id1 = Number(match2015[0]["id"]);
// let id2 = Number(match2015[match2015.length-1]["id"]);

// const filterdeliveries = deliveries.filter((item) =>{
//     let num = Number(item["match_id"]);
//     if(num>=id1 && num<=id2){
//         return item;
//     }
// })
// //console.log(filterdeliveries)
// let res1 = filterdeliveries.reduce((previous, current) => {
//     if(previous.hasOwnProperty(current["bowler"]));{
//         previous[current["bowler"]] += parseInt(current["total_runs"])
//     }
//         previous[current["bowler"]] = parseInt(current["total_runs"])
//     return previous
// },{})

// let res2 = filterdeliveries.reduce((previous, current) => {
//     if(previous.hasOwnProperty(current["bowler"])){
//         if(parseInt(current["ball"]) == "6"){
//             previous[current["bowler"]]+=1;
//         }
//     }
//     else{
//         previous[current["bowler"]] == 0;
//     }
//     return previous;
// }, {})
// console.log(res2)
// for(let property in res1){
//     res1[property] = res1[property]/res2[property];

// }

// const sort1 = Object.fromEntries(
//     Object.entries(res1).sort((a,b) => a-b)
// );
// //console.log(sort1)

// const economy = Object.keys(sort1).slice(0, 10).reduce((res, key) =>{
//     res[key] = sort1[key];
//     return res;
// },{})

// console.log(economy);
// //exporting into json file 
// // fs.writeFileSync(
// //     path.resolve(__dirname, "./economy.json"),
// //     JSON.stringify()

function findEconomicalBowlers(deliveries, matches) {
    let matchIds = {};
    matches.forEach((eachMatch, index) => {
      if (eachMatch["season"] === "2015") {
        matchIds[eachMatch["id"]] = true
      }
    })
  
    let bowlers = {};
  
    deliveries.map((eachDelivery, index) => {
      if (eachDelivery["match_id"] in matchIds) {
       
        if (bowlers[eachDelivery["bowler"]]) {
          bowlers[eachDelivery["bowler"]]["total_runs"] += parseInt(
            eachDelivery["total_runs"]
          );
  
          bowlers[eachDelivery["bowler"]]["total_balls"] += 1;
        } else {
          bowlers[eachDelivery["bowler"]] = {
            total_runs: parseInt(eachDelivery["total_runs"]),
            total_balls: 1,
          };
        }
      }
    });
    //console.log(bowlers)
    let averageOfBowlers = [];
  
    Object.keys(bowlers).map((eachBowler) => {
      let overs = bowlers[eachBowler]["total_balls"] / 6;
      let avg = bowlers[eachBowler]["total_runs"] / overs;
  
      averageOfBowlers.push([eachBowler, avg]);
    });
  
    averageOfBowlers.sort(function (a, b) {
      return a[1] - b[1];
    });
    let econ1 = averageOfBowlers.slice(0, 10);
    let econ = econ1.reduce((acc, curr) => {
        acc[curr[0]] = curr[1]
        return acc
    },{})
    return econ
  }
 
console.log(findEconomicalBowlers(deliveries, matches))

// //exporting into json file 
fs.writeFileSync(
 path.resolve(__dirname, "./economy.json"),
 JSON.stringify((findEconomicalBowlers(deliveries, matches))
 )
)

