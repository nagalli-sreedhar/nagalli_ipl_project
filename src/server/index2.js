const iplDataSet = require("./json");
const fs = require("fs");
const { PassThrough } = require("stream");
const path = require("path");//console.log(iplDataSet)

function matchesWon(year){
    let winners = {};
    for(let obj in iplDataSet.matches){
        if(iplDataSet.matches[obj]["season"] == year){
            if(winners.hasOwnProperty(iplDataSet.matches[obj]["winner"])){
                winners[iplDataSet.matches[obj]["winner"]] += 1;
            } else {
                winners[iplDataSet.matches[obj]["winner"]] = 1;
            }
        }
    }
    return winners;
    //console.log(winners);
}
let winners = {};
for (let ob in iplDataSet.matches){
  if (!winners.hasOwnProperty(iplDataSet.matches[ob]["season"]))
    winners[iplDataSet.matches[ob]["season"]] = matchesWon(iplDataSet.matches[ob]["season"]);
}
//exporting output to Json
console.log(winners);
fs.writeFileSync(
    path.resolve(__dirname, "./wonmatches.json"),
    JSON.stringify(winners)

)