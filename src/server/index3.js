const iplDataSet = require("./json");
const fs = require("fs");
const { PassThrough } = require("stream");
const path = require("path");


let extraRuns = {};
let matchId2016 = [];

for (let object in iplDataSet.matches){
  if(iplDataSet.matches[object]["season"] == 2016){
    matchId2016.push(iplDataSet.matches[object]["id"]);
  }
}

//console.log(matchId2016);
for (let ob in iplDataSet.deliveries){
  if(matchId2016.includes(iplDataSet.deliveries[ob]["match_id"])){
    if (!extraRuns.hasOwnProperty(iplDataSet.deliveries[ob]["bowling_team"]))
      extraRuns[iplDataSet.deliveries[ob]["bowling_team"]] = parseInt(iplDataSet.deliveries[ob]["extra_runs"]);
    else
      extraRuns[iplDataSet.deliveries[ob]["bowling_team"]] += parseInt(iplDataSet.deliveries[ob]["extra_runs"]);
  }
}
console.log(extraRuns);

fs.writeFileSync(
  path.resolve(__dirname, "./extrruns.json"),
  JSON.stringify(extraRuns)

)